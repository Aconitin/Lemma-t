#ifndef _utils_H
#define _utils_H

#include <cstdlib>
#include <iostream>
#include <vector>

template <typename T>
struct PointCloud
{

	std::vector<std::vector<T>> pts;
	std::vector<int> weights;

	// Must return the number of data points
	inline size_t kdtree_get_point_count() const { return pts.size(); }

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate value, the
	//  "if/else's" are actually solved at compile time.
	inline T kdtree_get_pt(const size_t idx, int dim) const
	{
		return pts[idx][dim];
	}
    inline std::vector<double> kdtree_get_vec(const size_t idx) const
	{
		return pts[idx];
	}
    inline int kdtree_get_weight(const size_t idx) const
	{
		return weights[idx];
	}
    inline std::vector<std::vector<T>> kdtree_get_all() const
	{
		return pts;
	}
    inline std::vector<int> kdtree_get_weights() const
	{
		return weights;
	}


	// Optional bounding-box computation: return false to default to a standard bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
	//   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }

};

template <typename T>
void addPoint(PointCloud<T> &pointCloud, std::vector<double> p, int weight=1)
{
	pointCloud.pts.push_back(p);
	pointCloud.weights.push_back(weight);
}
template <typename T>
void removePoint(PointCloud<T> &pointCloud, int idx)
{
	pointCloud.pts.erase(pointCloud.pts.begin() + idx);
	pointCloud.weights.erase(pointCloud.weights.begin() + idx);
}
template <typename T>
int removeAllZeroWeightPoints(PointCloud<T> &pointCloud)
{
    int removed = 0;
    int s = pointCloud.pts.size()-1;
    for(int i=s; i>=0; i--){
        if(pointCloud.weights[i] == 0){
            removePoint(pointCloud, i);
            removed++;
        }
    }
    return removed;
}
template <typename T>
void setWeightOfPoint(PointCloud<T> &pointCloud, size_t idx, int weight)
{
    pointCloud.weights[idx] = weight;
}

#endif
