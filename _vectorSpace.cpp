//> Includes!
#include "_vectorSpace.h"

namespace _vectorSpace{

    //> Given a point, returns nearest neighbour of that point!
    const tuple<size_t, double>
    singleSearch
    (vector<double>& queryPoint, wordVectorTreeType& wordVectorTree, int ignoreIndex){

        //> Find only the nearest neighbour for that query point!
        const size_t nrNeighbours = 2;
        vector<size_t> retIndices(nrNeighbours);
        vector<double> retDistSqrt(nrNeighbours);

        //> Do the search!
        nanoflann::KNNResultSet<double> resultSet(nrNeighbours);
        resultSet.init(&retIndices[0], &retDistSqrt[0]);
        wordVectorTree.index->findNeighbors(resultSet, &queryPoint[0], nanoflann::SearchParams(10));

        //> Return result!
        if (ignoreIndex == retIndices[0]){ //> If the closest word is the word we came from, ignore it and take the second closest!
            return make_tuple(retIndices[1], retDistSqrt[1]);
        }
        return make_tuple(retIndices[0], retDistSqrt[0]); //> Returns index of nearest point in wordVectors and distance (squared) to that!

    };

    //> Same as singleSearch but for multiple points!
    const tuple<vector<size_t>, vector<double>>
    multiSearch
    (vector<vector<double>>& queryPoints, wordVectorTreeType& wordVectorTree, vector<int>& ignoreIndices){

        vector<size_t> indices;
        vector<double> distances;

        //> Use singleSearch looping over the vector!
        for (unsigned i=0; i < queryPoints.size(); i++){
            size_t index;
            double distance;
            tie(index, distance) = singleSearch(queryPoints[i], wordVectorTree, ignoreIndices[i]);
            indices.push_back(index);
            distances.push_back(distance);
        }

        return make_tuple(indices, distances);

    };

    //> Conveniently prints the result set of a multi search!
    const void
    printMultiSearchResult
    (vector<size_t>& indices, vector<double>& distances, vector<string>& wordList){

        cout << "Printing multiSearch results:\n";
        for (unsigned i=0; i < indices.size(); i++){
                cout << "  Result " << i <<": " << wordList[indices[i]] << " | " << indices[i] << ", distance: " << distances[i] << "\n";
        }

    };

    //> Takes a vector and norms it!
    const void
    norm
    (vector<double>& v){

        double n = getNorm(v);
        for (unsigned i=0; i<v.size(); i++){
            v[i] = v[i] / n;
        }

    };

    //> Prints a vector!
    const void
    print
    (vector<double>& v){

        cout<<"[Vector, size: "<<v.size()<<", norm: "<<getNorm(v)<<"]\n";

    };

    //> Prints a vector verbose!
    const void
    printVerbose
    (vector<double>& v){

 //       cout<<"Vector (norm: "<<getNorm(v)<<"): ";
        for (unsigned i=0; i<v.size(); i++){
            cout<<"("<<i+1<<"): "<<v[i]<<", ";
        }
        cout<<"\n";

    };

    //> Adds two vectors together!
    const vector<double>
    getAddition
    (vector<double>& v1, vector<double>& v2){

        vector<double> result;
        transform(v1.begin(), v1.end(), v2.begin(), back_inserter(result), plus<float>());
        return result;

    };

    //> Subtracts two vectors!
    const vector<double>
    getSubtraction
    (vector<double>& v1, vector<double>& v2){

        vector<double> result;
        transform(v1.begin(), v1.end(), v2.begin(), back_inserter(result), minus<float>());
        return result;

    };

    //> Takes a vector and returns the vector times a scalar!
    const vector<double>
    getMultiplication
    (vector<double>& v1, double scalar){

        vector<double> v3;
        for (unsigned i=0; i<v1.size(); i++){
            v3.push_back(scalar * v1[i]);
        }
        return v3;

    };

    //> Takes two vectors and returns the average vector between them!
    const vector<double>
    getAverage
    (vector<double>& v1, vector<double>& v2){

        vector<double> v3;
        for (unsigned i=0; i<v1.size(); i++){
            v3.push_back(0.5 * (v1[i] + v2[i]));
        }
        return v3;

    };

    //> Calculates the vector dot product of two vectors!
    double
    getDotProduct
    (vector<double>& v1, vector<double>& v2){

        double dot = 0;
        for (unsigned i=0; i<v1.size(); i++){
            dot += (v1[i] * v2[i]);
        }
        return dot;

    };

    //> Calculates the euclidean distance of two vectors!
    double
    getDistance
    (vector<double>& v1, vector<double>& v2){

        double eu = 0;
        for (unsigned i=0; i<v1.size(); i++){
            eu += pow((v1[i] - v2[i]), 2);

        }
        return sqrt(eu);

    };

    //> Calculates the cosine similarity between two vectors!
    double
    getCosineSimilarity
    (vector<double>& v1, vector<double>& v2){
        double cs = (getDotProduct(v1, v2)) / ((getNorm(v1)) * (getNorm(v2)));
        return cs;
    };

    //> Returns the sum of all elements in a vector!
    const double
    getSumOfAllElements
    (vector<double>& v){

        double n = 0;
        for (unsigned i=0; i<v.size(); i++){
            n = n + v[i];
        }
        return n;

    };

    //> Returns the norm (length) of a vector as defined as sqrt(a^2 + b^2 + ...)!
    const double
    getNorm
    (vector<double>& v){

        double n = 0;
        for (unsigned i=0; i<v.size(); i++){
            n = n + pow(v[i], 2);
        }
        return sqrt(n);

    };

    //> Mixes two vectors together to create a third!
    const vector<double>
    getMix
    (vector<double>& v1, vector<double>& v2){

        vector<double> v3;
        for (unsigned i=0; i<v1.size(); i++){
                if (i%2==0){
                    v3.push_back(v1[i]);
                }else{
                    v3.push_back(v2[i]);
                }
        }
        return v3;

    };

    //> Pushes vector of vectors to file!
    const void pushToFileVectorOfVectors(vector<vector<double>>& v, string of){

        ofstream ofile; //> Output file!
        ofile.open(of);
        for(int i=0; i<v.size(); i++){
            string vec = "";
            for(unsigned k=0; k<v[i].size(); k++){
                vec = vec + to_string(v[i][k]);
                if(k == v[i].size()-1){
                    vec = vec + "\n";
                }else{
                    vec = vec + " ";
                }
            }
            ofile << vec;
        }
        ofile.close();

    };

    //> Pushes vector of ints to file!
    const void pushToFileVectorOfInts(vector<int>& v, string of){

        ofstream ofile; //> Output file!
        ofile.open(of);
        for(int i=0; i<v.size(); i++){
            ofile << v[i] << "\n";
        }
        ofile.close();

    };

};
