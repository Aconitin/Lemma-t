#ifndef _fileInput_H
#define _fileInput_H

//> Includes!
#include "_parameters.h"
#include "_vectorSpace.h"
#include <vector>
#include <tuple>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <exception>

using namespace std;

//> Useful functions to read in files!
class _fileInput{

	//> Privates!
	private:
        _fileInput(){}; //> Disallow creating an instance of this class!

	//> Publics!
	public:
	    static tuple<vector<vector<double>>, vector<string>> readVectorFile(string filename, int maxVectors = _parameters::maxVectorsFileInput, int printEvery = _parameters::printEveryFileInput); //> Reads in a word vector file, gives back a vector of wordvectors!
        static vector<int> readWeightsFile(string filename);
	    static vector<vector<double>> readTVFile(string filename);
	    static void print(string txt, int level); //> Prints according to severity level!
	    static vector<double> convertStringVectortoDoubleVector(const vector<string>& stringVector); //> Helper function to convert string vector to double vector!

};

#endif

