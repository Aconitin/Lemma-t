#ifndef _createTV_H
#define _createTV_H

//> Includes!
#include "_parameters.h"
#include "_vectorSpace.h"
#include <vector>
#include <tuple>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <exception>
#include <random>
#include <tgmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//> Stuff for nanoflann!
#include "lib/nanoflann.hpp"
#include "lib/utils.h"
#include <ctime>
#include <cstdlib>
#include <iostream>

using namespace std;

//> Useful functions to read in files!
class _createTV{

	//> Privates!
	private:
        _createTV(){}; //> Disallow creating an instance of this class!

	//> Publics!
	public:
	    static tuple<std::vector<std::vector<double>>, std::vector<int>, int> reduce(std::vector<std::vector<double>>& oldTVs, std::vector<int>& weights, std::vector<std::vector<double>>& wordVectors, double searchRadius, int reduceUntil);
        static void sampleToFile(std::vector<std::vector<double>> wordVectors, std::vector<std::string> wordList, string filename, int n, int lowerIndex, int upperIndex);
        static void sample(std::vector<std::vector<double>>& wordVectors, std::vector<std::vector<double>>& TVs, std::vector<int>& weights, int lowerIndex, int upperIndex, int amt);
        static void sampleReduce(std::vector<std::vector<double>>& wordVectors, int repeats);

};

#endif


