#ifndef _graph_H
#define _graph_H

//> Includes!
#include "_parameters.h"
#include "_vectorSpace.h"
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <unordered_map>

//> Stuff for nanoflann!
#include "lib/nanoflann.hpp"
#include "lib/KDTreeVectorOfVectorsAdaptor.h"
#include <ctime>
#include <cstdlib>
#include <iostream>

//> Graph class!
class _graph{

	//> Privates!
	private:
        std::unordered_map<std::string, std::unordered_map<std::string, double>> edges;

	//> Publics!
	public:
	    void print();
	    void addEdge(std::string from, std::string to, double weight);
	    void generateEdges(int n, std::vector<std::string>& wordList, std::vector<std::vector<double>>& wordVectors, std::vector<std::vector<double>>& TVs, std::vector<int>& weights);
        void dumpIntoFile(std::string filename);
};

#endif

