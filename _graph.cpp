#include "_graph.h"

//> Adds a weighted edge to the graph!
void _graph::addEdge(std::string from, std::string to, double weight){

    bool done = false;

    //> Check if "from" exists in the map already!
    if(edges.count(from) > 0){
        //> "from" was found, now check if edge from->to exists!
        if(edges[from].count(to) > 0){
            //> Edge from->to already exists, so just add the weight to the already existing weight!
            edges[from][to] = edges[from][to] + weight;
            done = true;
        }
    }

    //> Check the other way around if not already done!
    if(!done){
        //> Check if "to" exists in the map already!
        if(edges.count(to) > 0){
            //> "to" already exists!
            if(edges[to].count(from) > 0){
                //> Edge to->from exists, therefore add weight!
                edges[to][from] = edges[to][from] + weight;
            }else{
                //> Edge does not exist yet even though "to" exists, so create it!
                edges[to][from] = weight;
            }
        }else{
            //> "to" does not exist yet, so we have to create it all!
            edges[to][from] = weight;
        }
    }

};

//> Prints the whole thing!
void _graph::print(){
    for(auto i1 : edges){
            std::cout << i1.first << "\n";
        for(auto i2 : i1.second){
            std::cout << "  " << i2.first << " " << i2.second << "\n";
        }
    }
}

//> Generated a number of random word (vector) edges!
void _graph::generateEdges(int n, std::vector<std::string>& wordList, std::vector<std::vector<double>>& wordVectors, std::vector<std::vector<double>>& TVs, std::vector<int>& weights){

    std::cout << "    Creating index ...\n";

    //> Construct a kd-tree index for the wordVectors!
	typedef KDTreeVectorOfVectorsAdaptor< std::vector<std::vector<double>>, double >  wordVectorTreeType;
	wordVectorTreeType wvspace(wordVectors[0].size(), wordVectors, _parameters::maxLeafes); //> Dimensionality, Vectors, maxLeaf!
	wvspace.index->buildIndex();

	//> Initialize distributions!
	std::random_device rd;  //> Will be used to obtain a seed for the random number engine!
    mt19937 reInt(rd()); //> Random-number engine used (Mersenne-Twister in this case)!
    uniform_int_distribution<int> WVDist(0, wordList.size()-1); //> Uniform distribution for the wordVectors!
    uniform_int_distribution<int> TVDist(0, TVs.size()-1); //> Uniform distribution for the TVs!

    std::cout << "    Done! Creating edges now!\n";

	//> Generate edges!
    if(!_parameters::graphExhaustive){
        for(int i=1; i<=n; i++){

            //> Get a random word, random TV and its weight!
            int indexWord = WVDist(reInt);
            int indexTV = TVDist(reInt);
            std::vector<double>& TV = TVs[indexTV];

            //> Generate a new query point!
            std::vector<double> queryPoint = _vectorSpace::getAddition(wordVectors[indexWord], TV);
            if (_parameters::useUnitVectors == true){ //> Norm the vector!
                _vectorSpace::norm(queryPoint);
            }

            //> Search for the nearest neighbour to that query point!
            const size_t nrNeighbours = 2; //> +1 because itself is a neighbour!
            std::vector<size_t> retIndexes(nrNeighbours);
            std::vector<double> retDistSqrt(nrNeighbours);
            nanoflann::KNNResultSet<double> resultSet(nrNeighbours);
            resultSet.init(&retIndexes[0], &retDistSqrt[0]);
            wvspace.index->findNeighbors(resultSet, &queryPoint[0], nanoflann::SearchParams(10));

            //> Add an edge to the graph from the original word to that nearest neighbour!
            addEdge(wordList[indexWord], wordList[retIndexes[1]], (1.0 / retDistSqrt[1]) * weights[indexTV]);

            //> Print info!
            if(i%_parameters::graphPrintEvery == 0){
                std::cout << "    Created edge " + to_string(i) + "/" + to_string(_parameters::graphNrOfEdges) + "\n";
            }
        }
    }else{
        std::cout << "    Creating edges exhaustively, " << wordVectors.size() << " wordVectors * " << TVs.size() << " TVs!\n";

        for(int i1=0; i1<wordVectors.size(); i1++){
            for(int i2=0; i2<TVs.size(); i2++){

                //> Generate a new query point!
                std::vector<double> queryPoint = _vectorSpace::getAddition(wordVectors[i1], TVs[i2]);
                if (_parameters::useUnitVectors == true){ //> Norm the vector!
                    _vectorSpace::norm(queryPoint);
                }

                //> Search for the nearest neighbour to that query point!
                const size_t nrNeighbours = 2; //> +1 because itself is a neighbour!
                std::vector<size_t> retIndexes(nrNeighbours);
                std::vector<double> retDistSqrt(nrNeighbours);
                nanoflann::KNNResultSet<double> resultSet(nrNeighbours);
                resultSet.init(&retIndexes[0], &retDistSqrt[0]);
                wvspace.index->findNeighbors(resultSet, &queryPoint[0], nanoflann::SearchParams(10));

                //> Add an edge to the graph from the original word to that nearest neighbour!
                addEdge(wordList[i1], wordList[retIndexes[1]], (1.0 / retDistSqrt[1]) * weights[i2]);


            }
            std::cout << "    Done with wordVector " << i1+1 << "/" << wordVectors.size() << "\n";
        }
    }
};

//> Write whole graph into file like: EDGE1 EDGE2 WEIGHT \n!
void _graph::dumpIntoFile(std::string filename){

    ofstream ofile; //> Output file!
    ofile.open(_parameters::generalOutputFolder + filename);

    //> Iterate over all edges and dump them!
    for(auto i1 : edges){
        for(auto i2 : i1.second){
            ofile << i1.first << " " << i2.first << " " << i2.second << "\n";
        }
    }

    ofile.close();

    if(_parameters::graphAlsoOutputMirroredVersion){

        ofstream ofile2; //> Output file!
        ofile2.open(_parameters::generalOutputFolder + "mirrored_" + filename);

        //> Iterate over all edges and dump them!
        for(auto i1 : edges){
            for(auto i2 : i1.second){
                ofile2 << i1.first << " " << i2.first << " " << i2.second << "\n";
                ofile2 << i2.first << " " << i1.first << " " << i2.second << "\n";
            }
        }
    }
}
