#ifndef _parameters_H
#define _parameters_H

#include <string>

//> Defines global variables and parameters!
namespace _parameters{

    //> Block activation!
    const bool mainCreateTVs = false; //> Toggles the TV creation block!
    const bool mainLoadWordVectors = true; //> Toggles loading wordVectors and wordList (vocab) from files!
    const bool mainLoadTVs = true; //> Toggles loading TVs from file!
    const bool mainLoadWeights = true; //> Toggles loading weights from file!
    const bool mainCreateGraph = true;

    //> General stuff!
    const std::string generalOutputFolder = "..\\Output\\";
    const bool useUnitVectors = true;

    //> Vector input file parameters!
    const std::string inputVectorFile = "..\\Resources - Wortvectors\\vectors01.vec"; //> Input file for wordvectors!
    const int vectorSize = 1000; //> Size of each word vector, hence also size of individual!
    const int verbosityFileInput = 2; //> 1-4, the more, the more verbose!
    const int maxVectorsFileInput = 15000; //> Number of vectors to load, -1 for all!
    const int printEveryFileInput = 1000; //> Debug print every x vectors loaded!
    const bool vectorInputIgnoreFirstLine = true; //> Remove info line before actual vectors!
    const bool vectorInputIgnoreLastVectorEntry = false; //> Removes last vector entry because those are often line breaks!

    //> K-d tree parameters!
    const int maxLeafes = 10; //> Maximum number of entries in a single leaf for the tree!

    //> Sample-reduce parameters!
    const int reduceUntil = 250; //> Keeps reducing until this amount of TVs is hit!
    const int reduceUntilWorking = 2500; //> Working amount of TVs!
    const int maxNrOfSamplesBeforeReducing = 5000; //> Amount of TVs that are created before the TVs are reduced!
    const double searchRadiusInitial = 1.0; //> Initial Search radius size!
    const double searchRadiusMult = 1.01; //> Growth rate of the reduce search radius!
    const int searchesPerReduceCall = 80;
    const int minSearchResultsToNotIncreaseRadius = 40;
    const int searchStreakGiveupThreshold = 100; //> Give up reducing after this many failed attempts!
    const int sampleFromIndex = 0; //> Range of which input word vectors to use for sampling!
    const int sampleToIndex = 14998; //> Range of which input word vectors to use for sampling (WARNING: MUST BE MAX maxVectorsFileInput-2)!
    const std::string sampleReduceTVsOutputFile = "TVsUNNORMED.txt";
    const std::string sampleReduceWeightsOutputFile = "weightsUNNORMED.txt";
    const int nrOfSRRepeats = 250;

    //> TVs loading from file!
    const std::string TVSetID = "02";
    const std::string TVsAndWeightsPrefix = "..\\Resources - TVs and Weights\\";
    const std::string TVsSuffix = " TVs.txt";
    const std::string WeightsSuffix = " Weights.txt";

    //> Graph creation!
    const std::string graphOutputFile = "gra2.ph";
    const int graphNrOfEdges = 1500000;
    const int graphPrintEvery = 1500;
    const int graphExhaustive = false; //> If activated, will create an edge for every TV-wV pair!
    const bool graphAlsoOutputMirroredVersion = false; //> When printing to file, this creates an additional graph file where every edge also has its counter-edge!

};

#endif


