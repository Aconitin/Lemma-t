//> Header guard!
#ifndef _vectorSpace_H
#define _vectorSpace_H

//> Includes!
#include <vector>
#include <tuple>
#include <iostream>
#include <tgmath.h>
#include <algorithm>
#include <functional>
#include <fstream>
#include <iostream>
#include <sstream>

//> Stuff for nanoflann!
#include "lib/nanoflann.hpp"
#include "lib/KDTreeVectorOfVectorsAdaptor.h"
#include <ctime>
#include <cstdlib>
typedef KDTreeVectorOfVectorsAdaptor<std::vector<std::vector<double>>, double >  wordVectorTreeType;

using namespace std;

namespace _vectorSpace{

    //> Nanoflann kd-tree searches!
    const tuple<size_t, double> //> Given a point, returns nearest neighbour of that point!
    singleSearch(vector<double>&, wordVectorTreeType&, int);

    const tuple<vector<size_t>, vector<double>> //> Same as singleSearch but for multiple points!
    multiSearch(vector<vector<double>>&, wordVectorTreeType&, vector<int>&);

    const void //> Conveniently prints the result set of a multi search!
    printMultiSearchResult(vector<size_t>&, vector<double>&, vector<string>&);

    //> Operations on vectors!
    const void //> Takes a vector and norms it!
    norm(vector<double>&);

    const void //> Prints a vector!
    print(vector<double>&);

    const void  //> Prints a vector verbose!
    printVerbose(vector<double>&);

    //> Operations with vectors!
    const vector<double> //> Adds two vectors together!
    getAddition(vector<double>&, vector<double>&);

    const vector<double> //> Subtracts two vectors!
    getSubtraction(vector<double>&, vector<double>&);

    const vector<double> //> Takes two vectors and returns the average vector between them!
    getAverage(vector<double>&, vector<double>&);

    const vector<double> //> Takes a vector and returns the vector times a scalar!
    getMultiplication(vector<double>& v1, double scalar);

    double //> Calculates the vector dot product of two vectors!
    getDotProduct(vector<double>&, vector<double>&);

    double //> Calculates the euclidean distance of two vectors!
    getDistance(vector<double>&, vector<double>&);

    double //> Calculates the cosine similarity between two vectors!
    getCosineSimilarity(vector<double>&, vector<double>&);

    const double //> Returns the sum of all elements in a vector!
    getSumOfAllElements(vector<double>&);

    const double //> Returns the norm of a vector as defined as sqrt(a^2 + b^2 + ...)!
    getNorm(vector<double>&);

    const vector<double> //> Mixes two vectors together to create a third!
    getMix(vector<double>&, vector<double>&);

    const void //> Pushes vector of vectors to file!
    pushToFileVectorOfVectors(vector<vector<double>>&, string);

    const void //> Pushes vector of ints to file!
    pushToFileVectorOfInts(vector<int>&, string);

};

#endif
