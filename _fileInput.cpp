#include "_fileInput.h"

using namespace std;

//> Reads in a word vector file (output of google wordToVec), returns tuple with vector of wordvectors and wordlist!
tuple<vector<vector<double>>, vector<string>> _fileInput::readVectorFile(string filename, int maxVectors, int printEvery){

    //> Debug!
    print("  Reading in vector file ...\n", 1);

    vector<vector<double>> wv; //> To be returned!
    vector<string> sv; //> To be returned!
    ifstream infile(filename); //> Try to open file!
    if(infile.is_open() == false){
        print("  Error: Could not open file: " + filename + "\n", 1);
    } else {

        print("  Inputfile " + filename + " opened!\n  Reading in vectors ...\n", 1);

        //> Iterate over file and get it line by line!
        string line;
        int i = 0;
        unsigned int vecSize = -1;
        while(getline(infile, line))
        {
            //> Stop if maxVectors have been loaded!
            if(i == maxVectors){
                break;
            }

            //> Load, but not first line!
            if (!(i == 0 && _parameters::vectorInputIgnoreFirstLine == true)){
                istringstream iss(line);
                vector<string> tokens{istream_iterator<string>{iss},
                                                istream_iterator<string>{}};

                //> Seperate first string in tokens vector from the rest (this is our word)!
                string word = tokens[0];
                tokens.erase(tokens.begin());

                //> Remove last vector entry!
                if(_parameters::vectorInputIgnoreLastVectorEntry == true){
                    tokens.erase(tokens.end()); //> Last token in the file is some weird line break!
                }

                //> Make sure all vectors are of the same size!
                if(vecSize == -1){
                    vecSize = tokens.size();
                }
                if(tokens.size() != vecSize){
                    print("Error: Vector has odd size!", 1);
                }

                //> Push data to _wordVector object!
                vector<double> w = _fileInput::convertStringVectortoDoubleVector(tokens);

                //> Transform to unit vector!
                if (_parameters::useUnitVectors == true){
                    _vectorSpace::norm(w);
                }

                //> Add to return vector!
                wv.push_back(w);
                sv.push_back(word);

            }
            i += 1;
            if(i%printEvery == 0){
                print("    " + to_string(i) + "/" + to_string(_parameters::maxVectorsFileInput) + "\n", 2);
            }
        }

        print("  Done! Number of word vectors loaded: " + to_string(wv.size()) + "\n", 1);

    } //> END if(infile.is_open() == false){!

    return make_tuple(wv, sv);
}

//> Reads in Weights file!
vector<int> _fileInput::readWeightsFile(string filename){

    vector<int> weights;
    ifstream inputFile(filename);

    cout << "  Trying to open weights file " << filename <<"\n";
    if (inputFile) {
        cout << "  File opened! Reading contents ...\n";
        int v;

        while(inputFile >> v){
            weights.push_back(v);
        }
        cout << "    Done!\n";
    inputFile.close();
    }else{
        cout << "ERR Cannot open weights input file!\n";
    }

    cout << "  Successfully read in " << weights.size() << " weights!\n";
    return weights;

};

//> Reads in TV file!
vector<vector<double>> _fileInput::readTVFile(string filename){

    vector<vector<double>> TVs; //> To be returned!
    ifstream infile(filename); //> Try to open file!
    if(infile.is_open() == false){
        std::cout << "  Error: Could not open file: " + filename + "\n";
    }else{
        std::cout << "  Inputfile " + filename + " opened!\n  Reading in vectors ...\n";

        //> Iterate over file and get it line by line!
        string line;
        int i = 0;
        unsigned int vecSize = -1;
        while(getline(infile, line))
        {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss},
                                            istream_iterator<string>{}};

            //> Make sure all vectors are of the same size!
            if(vecSize == -1){
                vecSize = tokens.size();
            }
            if(tokens.size() != vecSize){
                cout << "  Error: Vector has odd size!\n";
            }

            //> Push data to _wordVector object!
            vector<double> w = _fileInput::convertStringVectortoDoubleVector(tokens);

            //> Add to return vector!
            TVs.push_back(w);

        }
        cout << "  Done! Number of TVs loaded: " + to_string(TVs.size()) + "\n";
    }

    return TVs;

};

//> Helpers!
vector<double> _fileInput::convertStringVectortoDoubleVector(const vector<string>& stringVector){
    vector<double> doubleVector(stringVector.size());
    transform(stringVector.begin(), stringVector.end(), doubleVector.begin(), [](const string& val){
        return stod(val);
    });
    return doubleVector;
}

void _fileInput::print(string txt, int level){
    if(level <= _parameters::verbosityFileInput){
        cout << txt;
    }
}
