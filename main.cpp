//> Includes!
#include "_parameters.h"
#include "_vectorSpace.h"
#include "_fileInput.h"
#include "_createTV.h"
#include "_graph.h"

#include <tuple>

//> Stuff for nanoflann!
#include "lib/nanoflann.hpp"
#include "lib/utils.h"
#include <ctime>
#include <cstdlib>
#include <iostream>

int main()
{

    //> Begin!
    std::cout << "Startup ...\n";
    srand(time(NULL));
    std::vector<std::string> wordList;
    std::vector<std::vector<double>> wordVectors;
    std::vector<std::vector<double>> TVs;
    std::vector<int> weights;
    std::cout << "OK\n\n";

    //> Load wordVectors and wordList out of some google word2vec wordVector file!
    if(_parameters::mainLoadWordVectors){
        std::cout << "Loading wordvectors ...\n";
        tie(wordVectors, wordList) = _fileInput::readVectorFile(_parameters::inputVectorFile);
        std::cout << "OK\n\n";
    }else{
        std::cout << "DEAC Skipping loading wordVectors and wordList from files!\n\n";
    }

    //> Create transformation vectors through sample-reduce!
    if(_parameters::mainCreateTVs){
        std::cout << "Creating transformation vectors through sample-reduce ...\n";
        _createTV::sampleReduce(wordVectors, _parameters::nrOfSRRepeats);
        std::cout << "OK\n\n";
    }else{
        std::cout << "DEAC Skipping creation of TVs through sample-reduce!\n\n";
    }

    //> Load the set of TVs from file!
    if(_parameters::mainLoadTVs){
        std::cout << "Loading transformation vectors from file ...\n";
        TVs = _fileInput::readTVFile(_parameters::TVsAndWeightsPrefix + _parameters::TVSetID + _parameters::TVsSuffix);
        std::cout << "OK\n\n";
    }else{
        std::cout << "DEAC Skipping loading of TVs from file!\n\n";
    }

    //> Load the set of weights from file!
    if(_parameters::mainLoadWeights){
        std::cout << "Loading weights from file ...\n";
        weights = _fileInput::readWeightsFile(_parameters::TVsAndWeightsPrefix + _parameters::TVSetID + _parameters::WeightsSuffix);
        std::cout << "OK\n\n";
    }else{
        std::cout << "DEAC Skipping loading of weights from file!\n\n";
    }

    //> Sanity checks!
    std::cout << "Doing sanity checks ...\n";
    if(_parameters::mainLoadWordVectors){
        if(wordList.size() != wordVectors.size()){
            std::cout << "ERR: wordVectors and wordList have different sizes!";
        }
    }
    if(_parameters::mainLoadWeights && _parameters::mainLoadTVs){
        if(weights.size() != TVs.size()){
            std::cout << "ERR: weights and TVs have different sizes!";
        }
    }
    std::cout << "OK\n\n";

    //> Create graph!
    if(_parameters::mainCreateGraph){
        std::cout << "Creating graph ...\n";
        _graph g = _graph();
        std::cout << "  Generating edges ...\n";
        g.generateEdges(_parameters::graphNrOfEdges, wordList, wordVectors, TVs, weights);
        std::cout << "  Done! Writing to file ...\n";
        g.dumpIntoFile(_parameters::graphOutputFile);
        std::cout << "  Done!\n";
        std::cout << "OK\n\n";
    }else{
        std::cout << "DEAC Skipping creation of graph!\n\n";
    }

	return 0;
}
