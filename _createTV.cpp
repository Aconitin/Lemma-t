#include "_createTV.h"

using namespace std;

void
_createTV::sampleToFile(std::vector<std::vector<double>> wordVectors, std::vector<std::string> wordList, string filename, int n, int lowerIndex, int upperIndex){
    uniform_int_distribution<int> uniInt(lowerIndex, upperIndex); //> Uniform distribution!
    mt19937 reInt; //> Random-number engine used (Mersenne-Twister in this case)!
    ofstream ofile; //> Output file!

    ofile.open(filename);
    for (int i=0; i<n; i++){
        int index1 = uniInt(reInt);
        int index2 = uniInt(reInt);
        while(index1 == index2){
            index2 = uniInt(reInt);
        }
        vector<double> TV = _vectorSpace::getSubtraction(wordVectors[index1], wordVectors[index2]);
 //       if (_parameters::useUnitVectors == true){ //> Norm the vector!
 //           _vectorSpace::norm(rTV);
 //       }
        ofile << wordList[index1] << " " << wordList[index2] << " ";
        string vec = "";
        for (unsigned k=0; k<TV.size(); k++){
            vec = vec + to_string(TV[k]);
            if(k == TV.size()-1){
                vec = vec + "\n";
            }else{
                vec = vec + " ";
            }
        }
        ofile << vec;
    }
     ofile.close();
};

void _createTV::sample
(std::vector<std::vector<double>>& wordVectors, std::vector<std::vector<double>>& TVs, std::vector<int>& weights, int lowerIndex, int upperIndex, int amt){

    //> Initialize!
    cout<<"    Sampling "<<amt<<" samples ...";
    std::random_device rd;  //> Will be used to obtain a seed for the random number engine!
    mt19937 reInt(rd()); //> Random-number engine used (Mersenne-Twister in this case)!
    uniform_int_distribution<int> uniInt(lowerIndex, upperIndex); //> Uniform distribution!

    //> Sample n samples!
    for (int i=0; i<amt; i++){
        int index1 = uniInt(reInt);
        int index2 = uniInt(reInt);
        while(index1 == index2){
            index2 = uniInt(reInt);
        }
        vector<double> TV = _vectorSpace::getSubtraction(wordVectors[index1], wordVectors[index2]);
        if (_parameters::useUnitVectors == true){ //> Norm the vector!
            _vectorSpace::norm(TV);
        }
        TVs.push_back(TV);
        weights.push_back(1);
    }
    cout<<" OK!\n";

};

tuple<std::vector<std::vector<double>>, std::vector<int>, int> _createTV::reduce
(std::vector<std::vector<double>>& oldTVs, std::vector<int>& weights, std::vector<std::vector<double>>& wordVectors, double searchRadius, int reduceUntil){

    //> Initialize!
    PointCloud<double> TVs; //> PointCloud to store the samples in!

    //> Construct a k-d tree index for the samples PointCloud. This is needed later for radiusSearching reduction candidates!
	typedef nanoflann::KDTreeSingleIndexDynamicAdaptor<
		nanoflann::L2_Simple_Adaptor<double, PointCloud<double> > ,
		PointCloud<double>,
		_parameters::vectorSize
		> PointCloudIndexType;
    PointCloudIndexType pointCloudIndex(_parameters::vectorSize, TVs, nanoflann::KDTreeSingleIndexAdaptorParams(_parameters::maxLeafes));

    //> Add all old TVs to the PointCloud!
    for(int i=0;i<oldTVs.size();i++){
        addPoint(TVs, oldTVs[i], weights[i]);
    }

    //> Add everything to the index pointCloudIndex!
    pointCloudIndex.addPoints(0, TVs.kdtree_get_point_count()-1);
    cout<<"#TVs: "<<TVs.kdtree_get_point_count();

    //> Do a radius search from random point!
    std::random_device rd;  //> Will be used to obtain a seed for the random number engine!
    mt19937 reInt1(rd());
    uniform_int_distribution<int> uniInt1(0, TVs.kdtree_get_point_count()-1);

    std::vector<std::vector<std::pair<size_t, double>>> results;
    for(int turns = 0; turns<_parameters::searchesPerReduceCall; turns++){

        //> Create result set for this search!
        std::vector<std::pair<size_t, double>> indices_dists;
        nanoflann::RadiusResultSet<double,size_t> resultSet(searchRadius, indices_dists);

        //> Get query point!
        int ind1 = uniInt1(reInt1);
        std::vector<double> queryPoint = TVs.kdtree_get_vec(ind1);
        double* qry = &queryPoint[0];

        pointCloudIndex.findNeighbors(resultSet, qry, nanoflann::SearchParams());
        results.push_back(resultSet.m_indices_dists);

    }
    cout<<", #RSs: "<<results.size();

    int added = 0;
    for(int r=0; r<results.size(); r++){
        //> Get the average vector of the found search and add to point cloud!
        int baseIndex = results[r][0].first;
        int totalWeight = TVs.kdtree_get_weight(baseIndex);
        std::vector<double> baseVector = TVs.kdtree_get_vec(baseIndex);
        baseVector = _vectorSpace::getMultiplication(baseVector, totalWeight);
        setWeightOfPoint(TVs, baseIndex, 0);
        for(int i=1; i<results[r].size(); i++){
            int index = results[r][i].first;
            std::vector<double> other = TVs.kdtree_get_vec(index);
            other = _vectorSpace::getMultiplication(other, TVs.kdtree_get_weight(index));
            totalWeight = totalWeight + TVs.kdtree_get_weight(index);
            setWeightOfPoint(TVs, index, 0);
            baseVector = _vectorSpace::getAddition(baseVector, other);

        }
        if(totalWeight > 0){
            double scalar = 1.0/totalWeight;
            baseVector = _vectorSpace::getMultiplication(baseVector, scalar);
            if (_parameters::useUnitVectors == true){ //> Norm the vector!
                _vectorSpace::norm(baseVector);
            }
            addPoint(TVs, baseVector, totalWeight);
            added++;
        }
    }
    int removed = removeAllZeroWeightPoints(TVs);
    removed = removed - added;
    cout<<", #R: "<<removed<<", #TVPCae: "<<TVs.kdtree_get_point_count()<<"->"<<to_string(reduceUntil)<<"\n";
    return make_tuple(TVs.kdtree_get_all(), TVs.kdtree_get_weights(), removed);

};

void _createTV::sampleReduce
(std::vector<std::vector<double>>& wordVectors, int repeats=1){

    std::vector<std::vector<double>> TVs;
    std::vector<int> weights;

    for(int rep=1; rep<=repeats; rep++){

        cout<<"  SampleReduce loop "<<rep<<"/"<<repeats<<"\n";

        //> Sample a bunch of TVs!
        _createTV::sample(wordVectors, TVs, weights, _parameters::sampleFromIndex, _parameters::sampleToIndex, _parameters::maxNrOfSamplesBeforeReducing);

        //> Now reduce!
        int reduceStepCount = 0;
        int streak = 0;
        double searchRadius = _parameters::searchRadiusInitial;
        int reduceUntil = _parameters::reduceUntilWorking;
        if(rep == repeats){
            reduceUntil = _parameters::reduceUntil;
        }
        while(TVs.size() > reduceUntil){
            reduceStepCount++;
            cout<<"    ("<<rep<<"/"<<repeats<<") Reducing step "<<reduceStepCount<<"(r="<<searchRadius<<")"<<": ";
            int reduced;
            tie(TVs, weights, reduced) = _createTV::reduce(TVs, weights, wordVectors, searchRadius, reduceUntil);
            if(reduced <= _parameters::minSearchResultsToNotIncreaseRadius){
                searchRadius = searchRadius * _parameters::searchRadiusMult;
                streak++;
            }
            else{
                streak = 0;
            }
            if(streak >= _parameters::searchStreakGiveupThreshold){
                cout<<"ERROR: Gave up reducing!";
                break;
            }
        }

        //> Push to file!
        _vectorSpace::pushToFileVectorOfVectors(TVs, _parameters::sampleReduceTVsOutputFile);
        _vectorSpace::pushToFileVectorOfInts(weights, _parameters::sampleReduceWeightsOutputFile);

    }

};
